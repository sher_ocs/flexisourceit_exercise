<?php

use App\Model\Employee;

/**
 * Handles Api Request/Response
 *
 * @author Sherwin Ocubillo <sher_ocs@yahoo.com>
 * @version 1
 * @package FlexisourceIT
 */
require 'bootstrap.php';

class Api extends Controller
{
    protected $className = 'api';

    protected function saveEmployeeAction($param)
    {
        try
        {
            /**
             * @todo
             *
             * Do all parameters request validation
             */

            $employee_model = new Employee();

            $employee_model->getTable()->setName($param['name']);
            $employee_model->getTable()->setPhoneNumber($param['phone_number']);
            $employee_model->getTable()->setEmail($param['email']);
            $employee_model->getTable()->setType($param['type']);
            $employee_model->getTable()->setPassword(generatePassword(12, 1, 2, 3)); // $password is 12 characters: 6 lower-case letters, 1 upper-case letter, 2 numbers and 3 symbols

            $employee_model->save();

            /**
             * @todo
             *
             * Trigger event observer for sending email
             */

            $return = [
                "status" => "success",
                "message" => $employee_model->id . " was added"
            ];
        }
        catch(Exception $err)
        {
            $return = ["status" => "failed", "message" => $err->getMessage()];

            /**
             * @todo
             *
             * Trigger event observer for writing logs
             */
        }

        return json_encode($return);
    }
}

$class = new Api($_REQUEST);

if (isset($_REQUEST['m'])) {
    $method = $_REQUEST['m'];
    $class->$method($_REQUEST);
} else {
    $class->index($_REQUEST);
}
