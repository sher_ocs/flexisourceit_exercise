<?php

define('APPLICATION_ENV', getenv('APPLICATION_ENV') ? getenv('APPLICATION_ENV') : 'local');
define('BASE_PATH', realpath(dirname(__FILE__)));
define('APPLICATION_PATH', BASE_PATH.'/../app/');
define('VENDOR_PATH', BASE_PATH.'/../vendor/');
define('CONFIG_PATH', APPLICATION_PATH.'config');

define('ZEND_VERSION', '1.11.12');  // change zend version accordingly
define('ZEND_PATH', VENDOR_PATH.'zend/'.ZEND_VERSION);

set_include_path(ZEND_PATH.PATH_SEPARATOR.get_include_path());

if (APPLICATION_ENV == 'production') {
    ini_set('display_errors', 0);
} else {
    ini_set('display_errors', 1);
}

/**
 *  Autoload the Zend core libraries
 */
require_once ZEND_PATH.'/Zend/Loader/Autoloader.php';
Zend_Loader_Autoloader::getInstance();

/**
 *  Set up the application configuration file
 */
$config = new Zend_Config_Ini(CONFIG_PATH.'/api.ini', APPLICATION_ENV );
Zend_Registry::set("config", $config);

/**
 *  Register Classes Autoloader
 */
require VENDOR_PATH . '/autoload.php';

/**
 *  Initialize Database connection
 */
$dbAdapter = Zend_Db::factory($config->database);
Zend_Db_Table::setDefaultAdapter($dbAdapter);

/**
 * Include helper functions
 */
require_once APPLICATION_PATH . 'helpers.php';

/**
 * Include controller.php
 */
require APPLICATION_PATH . 'controller.php';