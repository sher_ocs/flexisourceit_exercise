<?php

use App\Model\Employee;

/**
 * Handles main page
 * 
 * @author Sherwin Ocubillo <sher_ocs@yahoo.com>
 * @version 1
 * @package FlexioutsourceIT
 */
require 'bootstrap.php';

class Index extends Controller
{
	protected $className = 'index';
	
	protected function indexAction($param)
	{
		echo $this->render('index/index.phtml', $param);
	}

	protected function registerAction($param)
    {
        /**
         * @todo
         *
         * Do all parameters request validation
         */

        $employee_model = new Employee();

        $employee_model->getTable()->setName($param['name']);
        $employee_model->getTable()->setPhoneNumber($param['phone_number']);
        $employee_model->getTable()->setEmail($param['email']);
        $employee_model->getTable()->setType($param['type']);

        $employee_model->save();

        /**
         * @todo
         *
         * Handle proper session assignment
         */
        $_SESSION["logged_in_user_id"] = $$employee_model->id;

        /**
         * @todo
         *
         * Trigger event observer for the following below.
         * - sending email
         * - audit logs
         */

        /**
         * @todo
         *
         * Handle proper redirection
         */
        header('location: dashboard.php');
    }
}

$class = new Index($_REQUEST);

if (isset($_REQUEST['m'])) {
	$method = $_REQUEST['m'];
	$class->$method($_REQUEST);
} else {
	$class->index($_REQUEST);
}
